let widgetSource; // src of the widget
let intervalId; // IntervalId of the polling mechanism
let variables = {}; // Object used for data coming from Oswald
let delaytimer;
let metadata = {}; // Data object for sending data to Oswald

window.addEventListener(
  'message',
  function receiveMessage(event) {
    if (event.data && event.data.metadata) {
      if (event.data.metadata) {
        fillTerminalHTML(event.data);
      }
      variables = event.data.metadata.variables;
      setMetadataChanges();
      clearElements();
      createElement();
    }
  },
  false,
);

function createElement() {
  let elMetadata = document.getElementById('metadata');
  let elVariables = document.getElementById('variables');

  let variablesP = document.getElementById('var');
  let metadataP = document.getElementById('met');

  for (key in variables) {
    let p = document.createElement('p');
    variablesP.appendChild(p).innerHTML = syntaxHighlight(
      JSON.stringify({ key: key, value: variables[key] }),
    );
  }

  for (key in metadata) {
    let p = document.createElement('p');
    metadataP.appendChild(p).innerHTML = syntaxHighlight(
      JSON.stringify({ key: key, value: metadata[key] }),
    );
  }

  elMetadata.appendChild(metadataP);
  elVariables.appendChild(variablesP);
}

function setMetadataChanges() {
  if (variables) {
    for (key in variables) {
      metadata[key] = variables[key];
    }
  }
}

function clearElements() {
  let elMetadata = document.getElementById('met');
  let elVariables = document.getElementById('var');

  elMetadata.innerHTML = '';
  elVariables.innerHTML = '';
}

function fillTerminalHTML(event) {
  console.info('Full event obj:', event);
  let elTerminal = document.getElementById('term');
  elTerminal.innerHTML = '';

  for (key in event.metadata) {
    let p = document.createElement('p');
    elTerminal.appendChild(p).innerHTML = syntaxHighlight(
      JSON.stringify({ key: key, value: event.metadata[key] }),
    );
  }
}

function syntaxHighlight(json) {
  json = json
    .replace(/&/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;');
  return (document.createElement('div').innerHTML = json.replace(
    /("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g,
    function (match) {
      var cls = 'number';
      if (/^"/.test(match)) {
        if (/:$/.test(match)) {
          cls = 'key';
        } else {
          cls = 'string';
        }
      } else if (/true|false/.test(match)) {
        cls = 'boolean';
      } else if (/null/.test(match)) {
        cls = 'null';
      }
      return '<span class="' + cls + '">' + match + '</span>';
    },
  ));
}

function sendDataToOswald() {
  metadata = { token: 'dkjfskldgjdslgjlsdk' };
  let dataToSend = { eventName: 'oswald-data', metadata: metadata };
  document
    .getElementById('oswald-widget')
    .contentWindow.postMessage(dataToSend, '*');
  if (Object.keys(metadata).length > 0) {
    alert('Data send to oswald:' + JSON.stringify(metadata));
  } else {
    alert('No data to send to oswald');
  }

  // metadata will inform Oswald of all changes on the website and every other change he needs to know
}
