# Oswald metadata tester

## Url parameters

- chatbotId
- env

Possible environments
--------------------------------------------------------
- prod
- local
- dev
- acc

> Use the oswald metadata 

Example url :path/to/file/index.html?`chatbotId=id&env=prod`